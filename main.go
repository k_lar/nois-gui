package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"sync"

	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"github.com/ebitengine/oto/v3"
)

const (
	sampleRate      = 44100
	channelNum      = 2
	bitDepthInBytes = 2
	bytesPerSample  = channelNum * bitDepthInBytes
	bufferSize      = sampleRate / 10 // Buffer size for 1/10th of a second of audio
)

var (
	volumeFactor = 0.5 // Volume adjustment factor
	decayFactor  = math.Exp(-1.0 / 1000.0)
	isPlaying    = true
	lastVolume   = 0.5 // Default volume
)

func whiteNoise(samples int) []float64 {
	noise := make([]float64, samples)
	for i := range noise {
		noise[i] = rand.Float64()*2 - 1 // Generate values in the range [-1, 1]
	}
	return noise
}

func applyFilter(samples []float64, prevFiltered float64, lowerBound, upperBound float64) ([]float64, float64) {
	filtered := make([]float64, len(samples))
	for i, value := range samples {
		if i == 0 {
			filtered[i] = decayFactor*prevFiltered + value
		} else {
			filtered[i] = decayFactor*filtered[i-1] + value
		}

		// Ensure the filtered value stays within the specified bounds
		if filtered[i] > upperBound {
			filtered[i] = upperBound
		} else if filtered[i] < lowerBound {
			filtered[i] = lowerBound
		}
	}
	return filtered, filtered[len(filtered)-1]
}

func leakyIntegrator(samples []float64, alpha float64, prevSample float64) ([]float64, float64) {
	integrated := make([]float64, len(samples))
	integrated[0] = alpha*prevSample + (1-alpha)*samples[0]
	for i := 1; i < len(samples); i++ {
		integrated[i] = alpha*integrated[i-1] + (1-alpha)*samples[i]
	}
	return integrated, integrated[len(integrated)-1]
}

func normalize(samples []float64) {
	max := 0.0
	for _, value := range samples {
		if abs(value) > max {
			max = abs(value)
		}
	}
	factor := 1.0 / max
	for i := range samples {
		samples[i] *= factor
	}
}

func abs(x float64) float64 {
	if x < 0 {
		return -x
	}
	return x
}

func convertToPCM(samples []float64) []byte {
	buffer := make([]byte, len(samples)*bytesPerSample)
	for i, sample := range samples {
		sampleValue := int16(sample * 32767.0 * volumeFactor)
		for c := 0; c < channelNum; c++ {
			buffer[i*bytesPerSample+c*2] = byte(sampleValue & 0xff)
			buffer[i*bytesPerSample+c*2+1] = byte(sampleValue >> 8)
		}
	}
	return buffer
}

func generateNoise(ctx context.Context, wg *sync.WaitGroup, player *oto.Player, writer *io.PipeWriter) {
	defer wg.Done()
	alpha := 0.70 // Adjust the alpha value as needed

	prevSample := 0.0
	prevFiltered := 0.0

	go func() {
		defer writer.Close()
		for {
			select {
			case <-ctx.Done():
				return
			default:
				totalSamples := bufferSize // Generate bufferSize samples at a time
				allSamples := make([]float64, 0, totalSamples)

				// Generate noise samples
				for len(allSamples) < totalSamples {
					whiteNoiseSamples := whiteNoise(bufferSize) // Generate white noise
					lowerBound := -20.0                         // Define your lower bound
					upperBound := 0.0                           // Define your upper bound

					filteredNoiseSamples, lastFiltered := applyFilter(whiteNoiseSamples, prevFiltered, lowerBound, upperBound)
					integratedSamples, lastSample := leakyIntegrator(filteredNoiseSamples, alpha, prevSample) // Apply leaky integrator with previous sample state
					normalize(integratedSamples)                                                              // Normalize the samples

					allSamples = append(allSamples, integratedSamples...)
					prevSample = lastSample     // Update the previous sample for the next iteration
					prevFiltered = lastFiltered // Update the previous filtered value for the next iteration
				}

				// Convert to PCM format
				buffer := convertToPCM(allSamples)

				// Write audio buffer to pipe
				_, err := writer.Write(buffer)
				if err != nil {
					log.Fatal(err)
				}
			}
		}
	}()

	player.Play()

	for {
		select {
		case <-ctx.Done():
			player.Pause()
			return
		default:
			// Continue playing
		}
	}
}

func adjustVolume(ctx context.Context, player *oto.Player, reader *io.PipeReader, volumeChan chan float64) {
	for {
		select {
		case <-ctx.Done():
			return
		case volume := <-volumeChan:
			lastVolume = volume // Store the last adjusted volume
			player.SetVolume(volume)
		}
	}
}

func main() {
	a := app.New()
	w := a.NewWindow("nois")

	sliderLabel := widget.NewLabel("Volume: 50")
	volumeSlider := widget.NewSlider(0, 100)
	volumeSlider.Step = 0.05
	volumeSlider.SetValue(50)
	volumeChan := make(chan float64)

	volumeSlider.OnChanged = func(value float64) {
		sliderLabel.SetText("Volume: " + fmt.Sprintf("%.0f", value))
		volumeChan <- value / 100
	}

	var (
		ctx          context.Context
		cancel       context.CancelFunc
		wg           sync.WaitGroup
		playPauseBtn *widget.Button
		reader       *io.PipeReader
		writer       *io.PipeWriter
		player       *oto.Player
		audioContext *oto.Context
		err          error
	)

	op := &oto.NewContextOptions{
		SampleRate:   sampleRate,
		ChannelCount: channelNum,
		Format:       oto.FormatSignedInt16LE,
	}

	audioContext, readyChan, err := oto.NewContext(op)
	if err != nil {
		log.Fatal(err)
	}
	<-readyChan

	playPauseBtn = widget.NewButton("Pause", func() {
		if isPlaying {
			playPauseBtn.SetText("Play")
			cancel()
			player.Close() // Close the player when paused
		} else {
			playPauseBtn.SetText("Pause")
			ctx, cancel = context.WithCancel(context.Background())

			reader, writer = io.Pipe()
			player = audioContext.NewPlayer(reader)

			// Start the volume adjustment goroutine
			go adjustVolume(ctx, player, reader, volumeChan)

			wg.Add(1)
			go generateNoise(ctx, &wg, player, writer)

			// Set the player volume to the last adjusted volume
			player.SetVolume(lastVolume)
		}
		isPlaying = !isPlaying
	})

	content := container.NewVBox(
		widget.NewLabel("Nois 0.1.0"),
		sliderLabel,
		volumeSlider,
		playPauseBtn,
	)

	w.SetContent(content)
	ctx, cancel = context.WithCancel(context.Background())

	reader, writer = io.Pipe()
	player = audioContext.NewPlayer(reader)

	// Start the volume adjustment goroutine
	go adjustVolume(ctx, player, reader, volumeChan)

	wg.Add(1)
	go generateNoise(ctx, &wg, player, writer)

	w.ShowAndRun()

	cancel()
	wg.Wait()
}
